<div class="row">
    <div class="col-md-10">
        <?= $this->render('_form',['currency'=>$currency]) ?>
    </div>
</div>
<br>
<table class="table table-responsive">
    <thead>
    <tr>
        <th>#</th>
        <th>Валюта</th>
        <th>Курс по отношению к основной</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($currency_data as $datum):?>
        <tr>
            <td><?= $datum->id ?></td>
            <td><?= $datum->currency ?></td>
            <td><?= $datum->rate_to_main ?></td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>