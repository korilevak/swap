<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
Use yii\helpers\Url;
$form=ActiveForm::begin(['action'=>Url::to(['currency/save','layout'=>'horizontal'])]);?>
<?= $form->field($currency,'currency')->textInput(['placeholder'=>'USD'])->label('Валюта') ?>
<?= $form->field($currency,'rate_to_main')->textInput()->label('Курс по отношению к основной') ?>
<?= Html::submitButton('Добавить',['class'=>'btn btn-success']) ?>
<?php ActiveForm::end();?>
