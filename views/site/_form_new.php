<form action="<?= \yii\helpers\Url::to('site/distribute') ?>" method="post">
    <h4>Введите процентное соотнощение валют для распределения</h4>
    <?php foreach ($currencies as $currency) :?>
        <label><?= $c->currency ?></label> <?= Html::input('text','balance['.$c->id.']',0,['class'=>'form-control'])?>
    <?php endforeach;?>
    <div class="row text-center">
        <button class="btn btn-success">Перерастределить</button>
    </div>
</form>