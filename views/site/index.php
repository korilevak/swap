<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <form action="<?= Url::to(['api/distribute']) ?>" >
        <h3>Введите процентное соотнощение для растпределения</h3>
        <div class="row">
            <?php foreach ($currencies as $currency) :?>
            <div class="col-md-4">
                <label><?= $currency->currency ?></label>
                <input type="text" class="form-control" name="percentage[<?= $currency->currency ?>]" required placeholder="<?= 100/count($currencies) ?>%">
            </div>
            <?php endforeach;?>

        </div>
        <div class="row text-center " style="margin-top: 20px;">
           <div class="col-md-12">
               <button class="btn btn-success saveForm" >Распределить</button>
           </div>
        </div>
    </form>
    <br>
    <div class="row" id="now_than">
        <div class="col-md-10" id="now1"></div>
        <div class="col-md-10" id="than2"><?= $this->render('@app/views/exchange/balances.php',['exchanges'=>$exchanges]) ?></div>
    </div>
</div>

