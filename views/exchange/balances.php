<?php
$i=1;
?>
<table class="table table-sm table-responsive">
    <thead>
    <tr>
        <th>#</th>
        <th>Название</th>
        <th>Баланс</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($exchanges as $exchange):?>
        <tr>
            <td><?= $i++; ?></td>
            <td><?= $exchange['name']; ?></td>

            <td><?php
                foreach ($exchange['balances'] as $key=>$balance){
                    echo $key. " ".$balance. "; ";
                }
                ?></td>

        </tr>
    <?php endforeach;?>
    </tbody>

</table>