<?php
use yii\helpers\Html;
use yii\helpers\Url;

$form=\yii\bootstrap\ActiveForm::begin(['action'=>Url::to(['exchange/save'])])
?>
<?= $form->field($exchange,'name')->textInput() ?>
<?= $form->field($exchange,'api_key')->textInput() ?>
<div class="row">
    <div class="col-md-12">
        <h4 >Баланс</h4>
    </div>
    <?php if(!count($currency)):?>
    <div class="alert alert-warning">
        <a href="<?= Url::to(['currency/index']) ?>">Добавить валюту</a>
    </div>

    <?php else:?>
        <?php foreach ($currency as $c):?>
            <div class="col-md-4" style="margin-top: 10px;">
               <label><?= $c->currency ?></label> <?= Html::input('text','balance['.$c->id.']',0,['class'=>'form-control'])?>
            </div>
        <?php endforeach;?>
    <?php endif;?>
</div>
<br>

<?= Html::submitButton('Добавить биржу',['class'=>'btn btn-success']) ?>
<?php \yii\bootstrap\ActiveForm::end();?>
