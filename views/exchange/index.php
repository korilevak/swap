<?php
use yii\helpers\Url;
$i=1;
?>

<div class="row">
    <div class="col-md-10">
        <?= $this->render('_form',['exchange'=>$exchange,'currency'=>$currency]) ?>
    </div>

</div>
<hr>
<br>
<div class="row">
    <table class="table table-sm table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>API ключ</th>
            <th>Баланс</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($exchange_data as $ed):?>
            <tr>
                <td><?= $i++; ?></td>
                <td><?= $ed->name; ?></td>
                <td><?= $ed->api_key; ?></td>
                <td><?php
                    foreach ($ed->exchangeBalances as $exchangeBalance){
                        echo $exchangeBalance->balance. " ".$ed->getCurrencies()->where(['id'=>$exchangeBalance->currency_id])->one()->currency. "; ";
                    }
                    ?></td>
                <td>
                    <a href="<?= Url::to(['exchange/delete','id'=>$ed->id]) ?>">Удалить</a>
                </td>
            </tr>
        <?php endforeach;?>
        </tbody>

    </table>

</div>