<?php
/**
 * Created by PhpStorm.
 * User: N_Juraev
 * Date: 01.09.2019
 * Time: 8:45
 */

namespace app\models;

use yii\db\ActiveRecord;

class Exchange extends ActiveRecord
{
    public $balance;

    public function rules()
    {
        return [
          [['name','api_key'],'required'],
          ['name','unique','targetAttribute'=>'name']
        ];
    }

    public function getExchangeBalances()
    {
        return $this->hasMany(ExchangeBalance::className(),['exchange_id'=>'id']);
    }

    public function getCurrencies(){
        return $this->hasMany(Currency::className(), ['id' => 'currency_id'])
            ->via('exchangeBalances');
    }
}