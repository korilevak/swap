<?php
/**
 * Created by PhpStorm.
 * User: N_Juraev
 * Date: 01.09.2019
 * Time: 8:47
 */

namespace app\models;

use yii\db\ActiveRecord;

class Currency extends ActiveRecord
{

    public function rules()
    {

        return [
            [['currency','rate_to_main'],'required'],
            ['currency','string'],
            ['currency','unique','targetAttribute'=>'currency']
        ];
    }
}