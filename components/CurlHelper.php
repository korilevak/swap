<?php
/**
 * Created by PhpStorm.
 * User: N_Juraev
 * Date: 31.05.2018
 * Time: 10:30
 */

namespace app\components;

use Yii;
class CurlHelper
{

    static function  curl_get($action,array $params){
        //$ip=Yii::app()->request->userHostAddress;
        $curl=curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_URL, Yii::$app->params['api_url'].$action."?".http_build_query($params));
        $content = curl_exec( $curl );
        $err     = curl_errno( $curl );
        $errmsg  = curl_error( $curl );
        $header  = curl_getinfo( $curl );
        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;

        return $header;
    }

}