<?php
/**
 * Created by PhpStorm.
 * User: N_Juraev
 * Date: 01.09.2019
 * Time: 8:42
 */

namespace app\controllers;

use Yii;
use app\models\Currency;
use app\models\Exchange;
use app\models\ExchangeBalance;

class ExchangeController extends SiteController
{
    /**
     * Exchange index page action.
     *
     * @return view
     */
    public function actionIndex()
    {
        $exchange = new Exchange();
        $exchange_data = Exchange::find()->all();
        $currency = Currency::find()->all();

        return $this->render('index', ['exchange' => $exchange, 'exchange_data' => $exchange_data, 'currency' => $currency]);
    }

    /**
     * Save new exchange. Save exchange balance in loop
     *
     * @return redirect back
     */
    function actionSave()
    {
        $exchange = new Exchange();

        if ($exchange->load(Yii::$app->request->post()) && $exchange->validate()) {
            $exchange->save();

            foreach (Yii::$app->request->post('balance') as $key => $item) {
                $exchangeBalance = new ExchangeBalance();
                $exchangeBalance->exchange_id = $exchange->id;
                $exchangeBalance->currency_id = $key;
                $exchangeBalance->balance = $item;
                $exchangeBalance->save();
            }
            return $this->redirectBack('success', 'Данные сохранены');
        } else
            return $this->redirectBack('error', 'Не удалось сохранить данные');
    }

    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        if (!$id)
            $this->redirectBack('error', 'Неизвестный ID');

        $exchange = Exchange::findOne(['id' => $id]);
        ExchangeBalance::deleteAll(['exchange_id' => $exchange->id]);
        $exchange->delete();

        return $this->redirectBack('success', 'Запись удалена');

    }
}