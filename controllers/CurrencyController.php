<?php
/**
 * Created by PhpStorm.
 * User: N_Juraev
 * Date: 01.09.2019
 * Time: 8:42
 */

namespace app\controllers;

use Yii;
use app\models\Currency;

class CurrencyController extends SiteController
{
    public function actionIndex()
    {
        $currency = new Currency();
        $data = Currency::find()->all();

        return $this->render('index', ['currency' => $currency, 'currency_data' => $data]);
    }

    public function actionSave()
    {
        $currency = new Currency();

        if ($currency->load(Yii::$app->request->post()) && $currency->validate()) {
            $currency->currency = strtoupper($currency->currency);
            // если в базе уже указана основная валюта необходимо его изменить на обычную
            $currency->save();

            return $this->redirectBack('success', 'Данные сохранены');
        } else {

            return $this->redirectBack('error', 'Не удалось сохранить данные');
        }

    }
}