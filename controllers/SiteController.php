<?php

namespace app\controllers;

use app\components\CurlHelper;
use app\models\Currency;
use app\models\Exchange;
use app\Traits\ApiTrait;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    use ApiTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $currencies = Currency::find()->all();
        $exchanges = Exchange::find()->all();
        $res = [];
        $i = 0;
        foreach ($exchanges as $exchange) {
            $res[] = [
                'name' => $exchange->name,
                'balances' => $this->getBalances($exchange->api_key)
            ];
        }
        //$this->debug($res,1);
        return $this->render('index', ['currencies' => $currencies, 'exchanges' => $res]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function redirectBack($status, $message)
    {
        Yii::$app->session->setFlash($status, $message);
        return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
    }

    /**
     * helper for debugging data
     */
    public function debug($arr, $die = 0)
    {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
        if ($die)
            die();
    }


}
