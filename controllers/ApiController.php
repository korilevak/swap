<?php
/**
 * Created by PhpStorm.
 * User: N_Juraev
 * Date: 01.09.2019
 * Time: 15:02
 */

namespace app\controllers;

use app\Traits\ApiTrait;
use Yii;
use app\models\Exchange;

class ApiController extends SiteController
{
    use ApiTrait;

    public function actionDistribute()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $percentage = Yii::$app->request->post('percentage');

        if (!$this->check_percentage($percentage)) {
            Yii::$app->response->statusCode = 422;
            return ['status' => 'error', 'message' => 'Процентное соотнощение в сумме должно быть 100'];
        }
        $exchanges = Exchange::find()->all();
        $balances = [];
        $res = [];

        foreach ($exchanges as $exchange) {
            $balances[$exchange->api_key]['balances'] = $this->getBalances($exchange->api_key);
            $balances[$exchange->api_key]['distributed'] = $this->distributeByPercentage($exchange->api_key, $percentage, $balances[$exchange->api_key]['balances']);
            $res[] = [
                'name' => $exchange->name,
                'balances' => $balances[$exchange->api_key]['distributed']
            ];
        }
        $view = $this->renderAjax('@app/views/exchange/balances', ['exchanges' => $res]);

        return ['success' => true, 'html' => $view];
    }

    /**
     * percentage must equal 100
     **/
    private function check_percentage($percentage)
    {
        $sum = 0;

        foreach ($percentage as $perc) {
            $sum += $perc;
        }

        return ($sum == 100) ? true : false;
    }

    /**
     * @returns distributed balances by pecentage
     */
    private function distributeByPercentage($api_key, $percentage, $balances)
    {
        $summ = 0;
        $mustbe = [];// variable for calculating target balance
        $difference = [];//variable for difference between current and must be

        foreach ($balances as $key => $balance) {
            /**converting all curencies to main*/
            $price = $this->getPrice($key);
            $conv = round(($balance / $price[$key]), 4);
            $summ += $conv;
            $converted[$key] = $conv;
        }

        foreach ($percentage as $key => $percent) {
            $mustbe[$key] = ($summ * $percent) / 100;
            $difference[$key] = $converted[$key] - $mustbe[$key];
        }
        /***Сначала пройдемся по балансам на которых имеется излишка
         *лишние средства конвертируем в основную валюта и дальше из основной распределяем тек у кого нехватает
         */
        foreach ($difference as $key => $item) {
            if ($item < 0 || $key == Yii::$app->params['mainCurrency'])
                continue;
            // меняем излишек на основную
            $this->buy($api_key, $key . Yii::$app->params['mainCurrency'], $item);
            unset($difference[$key]);
            /** удаляем обработанную валюту*/
        }

        /** Конвертируем из основной валюты недостаток на других валютах */
        foreach ($difference as $key => $item) {
            if ($key == Yii::$app->params['mainCurrency'])
                continue;
            /* умножая на -1  мы убираем "-" */
            $this->buy($api_key, Yii::$app->params['mainCurrency'] . $key, $item * (-1));
        }
        return $this->getBalances($api_key);

    }
}