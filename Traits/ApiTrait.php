<?php

namespace app\Traits;

use app\models\Exchange;
use app\models\Currency;
use app\models\ExchangeBalance;
use Yii;

trait ApiTrait
{
    /***
     * get currency rate/price
     * @returns array
     * $key will be used for returning price for custom exchange
     * $currency, example "RUBUSD"
     */
    public function getPrice($currency, $key = null)
    {
        //Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        $main = substr($currency, 3);
        $secondary = substr($currency, 0, 3);
        $model = Currency::find()->where(['currency' => $secondary])->one();

        return [$currency => $model->rate_to_main];
    }

    /**buy currency
     * @returns array
     */
    public function buy($key, $currency, $price)
    {
        // Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        $currency = strtoupper($currency);
        $exchange = Exchange::find()->where(['api_key' => $key])->one();

        if (empty($exchange->id))
            return ['status' => 'error', 'message' => 'Неправильный ключ API'];

        $iz = Currency::find()->where(['currency' => substr($currency, 0, 3)])->one();
        $v = Currency::find()->where(['currency' => substr($currency, 3)])->one();

        if (empty($iz->id) || empty($v->id))
            return ['status' => 'error', 'message' => 'Неизвестная валюта'];
        $exchangeBalanceIz = ExchangeBalance::find()->where(['exchange_id' => $exchange->id])->andWhere(['currency_id' => $iz->id])->one();
        $exchangeBalanceV = ExchangeBalance::find()->where(['exchange_id' => $exchange->id])->andWhere(['currency_id' => $v->id])->one();

        if ($iz->currency == Yii::$app->params['mainCurrency']) {

            if ($exchangeBalanceIz->balance - $price < 0)
                return ['status' => 'error', 'message' => 'Нехватка средств в ' . $iz->currency];
            /** iz=iz-price *Вычитаем средства из баланса. Умножение на курс не нужен так как эта валюта основная*/
            $exchangeBalanceIz->balance = $exchangeBalanceIz->balance - $price;
            $exchangeBalanceIz->update();
            /** v = v + price * kurs **/
            /** Средства из предидущей операции плюсуются с учетом курса **/
            $exchangeBalanceV->balance = $exchangeBalanceV->balance + $price * $v->rate_to_main;
            $exchangeBalanceV->update();

            return ['status' => 'success', 'message' => 'Покупка произведена'];
        } elseif ($v->currency == Yii::$app->params['mainCurrency']) {

            if ($exchangeBalanceIz->balance - $price * $iz->rate_to_main < 0)
                return ['status' => 'error', 'message' => 'Нехватка средств в ' . $iz->currency];
            /** iz = iz - price * kurs
             * Вычитаем средства из баланса. Умножаем на курс
             */
            $exchangeBalanceIz->balance = $exchangeBalanceIz->balance - $price * $iz->rate_to_main;
            $exchangeBalanceIz->update();
            /** v = v + price **/
            /** Средства из предидущей операции плюсуются  **/
            $exchangeBalanceV->balance = $exchangeBalanceV->balance + $price;
            $exchangeBalanceV->update();

            return ['status' => 'success', 'message' => 'Покупка произведена'];
        } else {
            return ['status' => 'error', 'message' => 'Нет основной валюты'];
        }
    }

    /**get exchange balances
     * @returns array
     **/
    public function getBalances($key)
    {
        /** For json response */
        //Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        $exchange = Exchange::find()->where(['api_key' => $key])->one();

        if (empty($exchange->id))
            return ['status' => 'error', 'message' => 'Неправильный ключ API'];
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT currency,balance 
                                              FROM currency,exchange_balance
                                              WHERE currency.id=exchange_balance.currency_id
                                              AND exchange_balance.exchange_id=" . $exchange->id);
        $results = $command->queryAll();
        $arr = [];

        foreach ($results as $result) {
            $arr[$result['currency']] = $result['balance'];
        }

        return $arr;
    }

}